from rest_framework import serializers
from collectioncentres.models import CollectionCentre, Unit, ItemCategory, ItemName, CollectionCentreInventorySummary, \
    InventoryLog


class CollectionCentreSerializer(serializers.ModelSerializer):
    class Meta:
        model = CollectionCentre
        fields = ['latitude', 'longitude', 'name', 'address', 'manager', 'modified_by', 'modified_by']


class UnitSerializer(serializers.ModelSerializer):
    class Meta:
        model = Unit
        fields = ['name']


class ItemCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = ItemCategory
        fields = ['name']


class ItemNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = ItemName
        fields = ['name']


class CollectionCentreInventorySummarySerializer(serializers.ModelSerializer):
    class Meta:
        model = CollectionCentreInventorySummary
        fields = ['collection_centre', 'item', 'unit', 'quantity', 'required_quantity', 'modified_date']


class InventoryLogSerializer(serializers.ModelSerializer):
    class Meta:
        model = InventoryLog
        fields = ['collection_centre', 'category', 'unit', 'quantity', 'donor_name', 'donor_address', 'added_by',
                  'added_date']
